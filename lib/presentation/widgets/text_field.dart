// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    Key? key,
    required this.controller,
    required this.labelText,
    this.focus = false,
    this.obscureText = false,
    required this.textInputType,
    this.textInputAction = TextInputAction.done,
    this.showCursor = true,
    this.hintText,
    this.onChanged,
    this.style,
    this.borderColor = false,
    this.checkerLength = 1,
    this.focusNode,
  }) : super(key: key);

  final TextEditingController controller;
  final String labelText;
  final bool obscureText;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final bool focus;
  final bool showCursor;
  final String? hintText;
  final Function(String value)? onChanged;
  final TextStyle? style;
  final bool borderColor;
  final int checkerLength;
  final FocusNode? focusNode;
  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  FocusNode focusNode = FocusNode();
  bool borderColor = false;
  @override
  void dispose() {
    super.dispose();
    focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Colors.grey,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(1),
        ),
        child: Center(
          child: Focus(
            focusNode: focusNode,
            onFocusChange: (hasFocus) {
              setState(() {
                if (hasFocus) {
                  borderColor = true;
                } else if (!hasFocus && widget.controller.text.isEmpty) {
                  borderColor = false;
                }
              });
            },
            child: TextFormField(
              focusNode: widget.focusNode,
              showCursor: widget.showCursor,
              autofocus: widget.focus,
              obscureText: widget.obscureText,
              textAlignVertical: TextAlignVertical.center,
              onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
              obscuringCharacter: '*',
              style: widget.style ??
                  const TextStyle(color: Colors.black, fontSize: 15),
              controller: widget.controller,
              minLines: 1,
              maxLines: 2,
              keyboardType: widget.textInputType,
              onChanged: widget.onChanged,
              textInputAction: widget.textInputAction,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 8, bottom: 8),
                hintText: widget.hintText,
                labelStyle: const TextStyle(color: Colors.grey, fontSize: 15),
                border: InputBorder.none,
                labelText: widget.labelText,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
