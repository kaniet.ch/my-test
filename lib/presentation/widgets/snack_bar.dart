import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String? text) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: const Duration(seconds: 1),
      backgroundColor: Colors.black,
      content: Text(
        text ?? 'Ошибка',
        textAlign: TextAlign.center,
      )));
}
