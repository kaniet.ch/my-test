import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/auth/auth_bloc.dart';
import '../../widgets/snack_bar.dart';
import '../../widgets/text_field.dart';
import '../home/home_screen.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Погода"),
        backgroundColor: Colors.orange,
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is AuthLoadingState) {
          } else if (state is AuthPostErrorState) {
            showSnackBar(context, state.error);
          } else if (state is AuthPostState) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const HomeScreen()),
                (Route<dynamic> route) => false);
          }
        },
        child: Column(
          children: [
            const SizedBox(height: 60.0),
            const Text(
              "Прогноз погоды 🌤️",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 10.0),
            CustomTextField(
              controller: emailController,
              labelText: 'Ваш e-mail',
              textInputType: TextInputType.emailAddress,
              onChanged: (value) {
                setState(() {});
              },
            ),
            const SizedBox(height: 24.0),
            CustomTextField(
              controller: passwordController,
              labelText: 'Ваш пароль',
              textInputType: TextInputType.text,
              onChanged: (value) {
                setState(() {});
              },
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: SizedBox(
                width: double.infinity,
                height: 60,
                child: ElevatedButton(
                  onPressed: completeFields
                      ? () {
                          context.read<AuthBloc>().add(PostDataEvent(
                              email: emailController.text,
                              password: passwordController.text));
                        }
                      : null,
                  style: ButtonStyle(
                    backgroundColor: completeFields
                        ? const MaterialStatePropertyAll<Color>(Colors.orange)
                        : const MaterialStatePropertyAll<Color>(Colors.grey),
                    elevation: const MaterialStatePropertyAll(0),
                  ),
                  child: const Text(
                    "Войти",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10)
          ],
        ),
      ),
    );
  }

  bool get completeFields {
    if (emailController.text.trim().length >= 3 &&
        passwordController.text.trim().length >= 3) {
      return true;
    }
    return false;
  }
}
