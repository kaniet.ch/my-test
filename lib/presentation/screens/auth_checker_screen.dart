import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/presentation/screens/auth/auth_screen.dart';
import 'package:weather_app/presentation/screens/home/home_screen.dart';
import 'package:weather_app/presentation/screens/loading/loading_screen.dart';

import '../bloc/auth_check/auth_check_bloc.dart';

class AuthCheckerScreen extends StatelessWidget {
  const AuthCheckerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCheckBloc, AuthCheckState>(
        builder: (context, state) {
      if (state is Authenticated) {
        return const HomeScreen();
      } else if (state is UnAuthenticated) {
        return const AuthScreen();
      }
      return const LoadingScreen();
    });
  }
}
