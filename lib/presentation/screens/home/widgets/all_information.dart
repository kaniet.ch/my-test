import 'package:flutter/material.dart';
import 'package:weather_app/data/model/weather_model.dart';
import 'package:weather_app/presentation/screens/home/widgets/weather_info.dart';

class AllInformation extends StatelessWidget {
  final WeatherModel weatherModel;
  const AllInformation({super.key, required this.weatherModel});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 40,
        ),
        Text(
          weatherModel.name,
          style: const TextStyle(fontSize: 19, color: Colors.black),
        ),
        const SizedBox(
          height: 16,
        ),
        Text(
          '${weatherModel.main.temp?.toInt().toString()}°',
          style: const TextStyle(fontSize: 120),
        ),
        const SizedBox(
          height: 16,
        ),
        Text(
          weatherModel.weather[0].main ?? "",
          style: const TextStyle(fontSize: 19),
        ),
        const SizedBox(
          height: 16,
        ),
        WeatherInfo(
          mainInfo: weatherModel,
        )
      ],
    );
  }
}
