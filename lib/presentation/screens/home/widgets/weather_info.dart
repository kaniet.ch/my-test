import 'package:flutter/material.dart';
import 'package:weather_app/data/model/weather_model.dart';
import 'package:weather_app/presentation/screens/home/widgets/details.dart';

class WeatherInfo extends StatelessWidget {
  final WeatherModel mainInfo;
  const WeatherInfo({super.key, required this.mainInfo});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Column(
        children: [
          const SizedBox(height: 15),
          const Text(
            "Today details",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 15),
          Row(
            children: [
              Details(
                  value: mainInfo.main.humidity.toString(),
                  name: "Humidity",
                  icon: const Icon(
                    Icons.water_drop,
                    color: Colors.blue,
                  )),
              const SizedBox(width: 10),
              Details(
                  value: mainInfo.main.seaLevel.toString(),
                  name: "Sea level",
                  icon: const Icon(
                    Icons.upload,
                    color: Colors.blue,
                  )),
            ],
          ),
          const SizedBox(height: 15),
          Row(
            children: [
              Details(
                  value: mainInfo.main.pressure.toString(),
                  name: "Pressure",
                  icon: const Icon(
                    Icons.arrow_downward,
                    color: Colors.blue,
                  )),
              const SizedBox(width: 10),
              Details(
                  value: mainInfo.main.feelsLike.toString(),
                  name: "Feels like",
                  icon: const Icon(
                    Icons.thermostat_outlined,
                    color: Colors.blue,
                  )),
            ],
          ),
          const SizedBox(height: 15),
          Row(
            children: [
              Details(
                  value: mainInfo.main.tempMax.toString(),
                  name: "Temp. max",
                  icon: const Icon(
                    Icons.arrow_upward,
                    color: Colors.blue,
                  )),
              const SizedBox(width: 10),
              Details(
                  value: mainInfo.timezone.toString(),
                  name: "Timezone",
                  icon: const Icon(
                    Icons.alarm_outlined,
                    color: Colors.blue,
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
