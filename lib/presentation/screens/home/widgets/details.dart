import 'package:flutter/material.dart';

class Details extends StatelessWidget {
  const Details(
      {super.key, required this.value, required this.name, required this.icon});
  final String value;
  final String name;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      width: MediaQuery.of(context).size.width / 2 - 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14), color: Colors.blueGrey[50]),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          icon,
          const SizedBox(
            width: 12,
          ),
          Column(children: [
            Text(
              name,
            ),
            const SizedBox(height: 10),
            Text(
              value,
              style: const TextStyle(fontWeight: FontWeight.bold),
            )
          ]),
        ],
      ),
    );
  }
}
