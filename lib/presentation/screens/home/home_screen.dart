import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/presentation/bloc/home/home_bloc.dart';
import 'package:weather_app/presentation/screens/home/widgets/all_information.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    context.read<HomeBloc>().add(GetDataEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Погода в вашем регионе"),
        backgroundColor: Colors.orange,
      ),
      body: BlocConsumer<HomeBloc, HomeState>(listener: (context, state) {
        if (state is HomeGetDataState) {
          context
              .read<HomeBloc>()
              .add(SaveDataEvent(weatherModel: state.weatherModel.weather[0]));
        }
      }, builder: (context, state) {
        return SingleChildScrollView(
            child: state is HomeGetDataState
                ? AllInformation(weatherModel: state.weatherModel)
                : const Center(child: CircularProgressIndicator.adaptive()));
      }),
    );
  }
}
