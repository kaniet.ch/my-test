part of 'auth_bloc.dart';

abstract class AuthEvent {}

class PostDataEvent extends AuthEvent {
  final String email;
  final String password;

  PostDataEvent({required this.email, required this.password});
}
