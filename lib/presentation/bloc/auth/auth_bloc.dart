import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:weather_app/config/secure_storage_keys.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  FlutterSecureStorage secureStorage = const FlutterSecureStorage();
  AuthBloc() : super(AuthInitial()) {
    on<PostDataEvent>((event, emit) {
      emit(AuthLoadingState());
      if (event.email.contains("test@test.org") &&
          event.password.contains("password")) {
        secureStorage.write(
            key: SecureStorageKeys.userToken,
            value: "6bcb7b7dfa873cbf430c50a7d3e87cb8");
        emit(AuthPostState());
      } else {
        emit(AuthPostErrorState("Неправильные данные"));
      }
    });
  }
}
