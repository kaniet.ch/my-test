part of 'auth_bloc.dart';

abstract class AuthState {}

class AuthInitial extends AuthState {}

class AuthLoadingState extends AuthState {}

class AuthPostState extends AuthState {}

class AuthPostErrorState extends AuthState {
  final String error;

  AuthPostErrorState(this.error);
}
