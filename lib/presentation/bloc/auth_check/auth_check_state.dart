part of 'auth_check_bloc.dart';

abstract class AuthCheckState {}

class Authenticated extends AuthCheckState {}

class UnAuthenticated extends AuthCheckState {}

class CheckInitial extends AuthCheckState {}
