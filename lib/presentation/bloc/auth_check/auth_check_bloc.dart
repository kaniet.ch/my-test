import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../config/secure_storage_keys.dart';
import '../../../utils/check_auth.dart';

part 'auth_check_event.dart';
part 'auth_check_state.dart';

class AuthCheckBloc extends Bloc<AuthCheckEvent, AuthCheckState> {
  FlutterSecureStorage secureStorage = const FlutterSecureStorage();
  AuthCheckBloc() : super(CheckInitial()) {
    on<AppStarted>((event, emit) async {
      CheckAuth.token =
          await secureStorage.read(key: SecureStorageKeys.userToken);
      if (CheckAuth.token != null) {
        emit(Authenticated());
      } else {
        emit(UnAuthenticated());
      }
    });
  }
}
