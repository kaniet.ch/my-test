import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:location/location.dart';
import 'package:weather_app/data/model/weather.dart';
import 'package:weather_app/data/model/weather_model.dart';
import 'package:weather_app/data/repository/weather_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final WeatherRepository _weatherRepository;
  HomeBloc(final WeatherRepository weatherRepository)
      : _weatherRepository = weatherRepository,
        super(HomeInitial()) {
    on<GetDataEvent>((event, emit) async {
      emit(HomeLoadingState());
      try {
        final locData = await Location().getLocation();
        final weatherData = await _weatherRepository.getWeather(
            lat: locData.latitude!, long: locData.longitude!);
        emit(HomeGetDataState(weatherModel: weatherData));
      } catch (e) {
        emit(HomeGetDataErrorState(e.toString()));
      }
    });
    on<SaveDataEvent>((event, emit) async {
      Box<Weather> factsBox = Hive.box<Weather>("local_data");
      factsBox.add(event.weatherModel);
    });
  }
}
