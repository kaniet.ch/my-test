part of 'home_bloc.dart';

abstract class HomeEvent {}

class GetDataEvent extends HomeEvent {}

class SaveDataEvent extends HomeEvent {
  final Weather weatherModel;

  SaveDataEvent({required this.weatherModel});
}
