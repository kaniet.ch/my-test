part of 'home_bloc.dart';

abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeLoadingState extends HomeState {}

class HomeGetDataState extends HomeState {
  final WeatherModel weatherModel;

  HomeGetDataState({required this.weatherModel});
}

class HomeGetDataErrorState extends HomeState {
  final String error;

  HomeGetDataErrorState(this.error);
}
