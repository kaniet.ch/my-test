import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:weather_app/data/model/weather_model.dart';
part 'network_service.g.dart';

@RestApi(baseUrl: "https://api.openweathermap.org/")
abstract class RestClient {
  factory RestClient(Dio dio, {String? baseUrl}) = _RestClient;

  @GET("/data/2.5/weather?units=metric&APPID=6bcb7b7dfa873cbf430c50a7d3e87cb8")
  Future<WeatherModel> getWeather(
      @Query('lon') double long, @Query('lat') double lat);
}
