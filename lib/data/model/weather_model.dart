import 'package:json_annotation/json_annotation.dart';
import 'package:weather_app/data/model/main_info.dart';
import 'package:weather_app/data/model/weather.dart';
part 'weather_model.g.dart';

@JsonSerializable()
class WeatherModel {
  @JsonKey(name: "weather")
  final List<Weather> weather;

  @JsonKey(name: "base")
  final String base;

  @JsonKey(name: "main")
  final MainInfo main;

  @JsonKey(name: "visibility")
  final int visibility;

  @JsonKey(name: "dt")
  final int dt;

  @JsonKey(name: "timezone")
  final int timezone;

  @JsonKey(name: "id")
  final int id;

  @JsonKey(name: "name")
  final String name;

  @JsonKey(name: "cod")
  final int cod;

  WeatherModel(this.weather, this.base, this.main, this.visibility, this.dt,
      this.timezone, this.id, this.name, this.cod);

  factory WeatherModel.fromJson(Map<String, dynamic> json) =>
      _$WeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherModelToJson(this);
}
