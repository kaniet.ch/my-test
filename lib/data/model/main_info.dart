import 'package:json_annotation/json_annotation.dart';
part 'main_info.g.dart';

@JsonSerializable()
class MainInfo {
  @JsonKey(name: "temp")
  final double? temp;

  @JsonKey(name: "feels_like")
  final double? feelsLike;

  @JsonKey(name: "temp_min")
  final double? tempMin;

  @JsonKey(name: "temp_max")
  final double? tempMax;

  @JsonKey(name: "pressure")
  final int? pressure;

  @JsonKey(name: "humidity")
  final int? humidity;

  @JsonKey(name: "sea_level")
  final int? seaLevel;

  @JsonKey(name: "grnd_level")
  final int? grndLevel;

  MainInfo(
      {this.temp,
      this.feelsLike,
      this.tempMin,
      this.tempMax,
      this.pressure,
      this.humidity,
      this.seaLevel,
      this.grndLevel});

  factory MainInfo.fromJson(Map<String, dynamic> json) =>
      _$MainInfoFromJson(json);

  Map<String, dynamic> toJson() => _$MainInfoToJson(this);
}
