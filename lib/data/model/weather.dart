import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
part 'weather.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class Weather {
  @JsonKey(name: "id")
  @HiveField(0)
  final int id;

  @JsonKey(name: "main")
  @HiveField(1)
  final String? main;

  @JsonKey(name: "description")
  @HiveField(2)
  final String? description;

  @JsonKey(name: "icon")
  @HiveField(3)
  final String? icon;

  Weather({required this.id, this.main, this.description, this.icon});

  factory Weather.fromJson(Map<String, dynamic> json) =>
      _$WeatherFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);
}
