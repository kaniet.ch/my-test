import 'package:weather_app/core/error.dart';
import 'package:weather_app/data/model/weather_model.dart';
import '../../network/network_service.dart';

class WeatherRepository {
  final RestClient _restClient;
  WeatherRepository({required RestClient restClient})
      : _restClient = restClient;

  Future<WeatherModel> getWeather(
      {required double lat, required double long}) async {
    try {
      final result = await _restClient.getWeather(lat, long);
      return result;
    } catch (e) {
      throw ServerFailure(error: e.toString());
    }
  }
}
