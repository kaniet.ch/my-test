import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/data/repository/weather_repository.dart';
import 'package:weather_app/network/network_service.dart';
import 'package:weather_app/presentation/bloc/auth/auth_bloc.dart';
import 'package:weather_app/presentation/bloc/home/home_bloc.dart';
import '../presentation/bloc/auth_check/auth_check_bloc.dart';
import '../presentation/screens/auth_checker_screen.dart';

class WeatherApp extends StatefulWidget {
  const WeatherApp({super.key});

  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
            create: (context) =>
                HomeBloc(WeatherRepository(restClient: RestClient(Dio())))),
        BlocProvider<AuthBloc>(create: (context) => AuthBloc()),
        BlocProvider<AuthCheckBloc>(
            create: (context) => AuthCheckBloc()..add(AppStarted()))
      ],
      child: MaterialApp(
        title: 'Weather App',
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: const AuthCheckerScreen(),
      ),
    );
  }
}
